//
//  Player.swift
//  
//
//  Created by Denis Zakharov on 14/09/15.
//
//

import Foundation
import CoreData

class Player: NSManagedObject {

    @NSManaged var age: NSNumber?
    @NSManaged var name: String?
    @NSManaged var position: String?
    @NSManaged var team: Team?

}

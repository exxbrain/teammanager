//
//  Team.swift
//  
//
//  Created by Denis Zakharov on 14/09/15.
//
//

import Foundation
import CoreData
import UIKit

class Team: NSManagedObject {

    @NSManaged var prvColor: AnyObject?
    @NSManaged var name: String?
    @NSManaged var players: NSSet
    
    var color: UIColor {
        get {
            let data = self.prvColor as? NSData
            if (data == nil) {
                return UIColor.clearColor()
            }
            return NSKeyedUnarchiver.unarchiveObjectWithData(data!) as! UIColor
        }
        set {
            self.prvColor = NSKeyedArchiver.archivedDataWithRootObject(newValue)
        }
    }
    
    

}

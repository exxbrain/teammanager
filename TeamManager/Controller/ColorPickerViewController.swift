//
//  ColorPickerViewController.swift
//  
//
//  Created by Denis Zakharov on 13/09/15.
//
//

import UIKit
import ColorSlider

class ColorPickerViewController: UIViewController {

    //MARK: - Outlets and Actions
    
    @IBOutlet weak var resultView: UIView!
    
    @IBAction func cancelTapped(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func doneTapped(sender: AnyObject) {
        team?.color = self.color!
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: - Public properties

    var team: Team?

    //MARK: - Private properties
    
    private var color: UIColor?
    
    //MARK: - Lifcycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let color = team?.color
        self.color = color != nil ? color : UIColor.whiteColor()
        self.updateView()
    }
    
    private func updateView() {
        resultView.backgroundColor = color
    }

    @IBAction func colorChanged(sender: ColorSlider) {
        color = sender.color
        self.updateView()
    }

}

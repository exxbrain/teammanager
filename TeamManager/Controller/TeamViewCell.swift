//
//  TeamViewCell.swift
//  
//
//  Created by Denis Zakharov on 14/09/15.
//
//

import UIKit

class TeamViewCell: UITableViewCell {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func initWithTeam(team: Team) {
        self.colorView.backgroundColor = team.color
        self.nameLabel.text = team.name != nil ? team.name : ""
    }

}

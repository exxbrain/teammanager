//
//  TeamDetailsTableViewController.swift
//  
//
//  Created by Denis Zakharov on 13/09/15.
//
//

import UIKit
import CoreData

class TeamDetailsTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    
    // MARK: - Outlets & Actions
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var colorView: UIView!
    
    // MARK: - Public properties

    var context : NSManagedObjectContext?
    var team: Team!
    
    // MARK: - Lifcycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
        var error: NSError? = nil
        if (fetchedResultsController.performFetch(&error) == false) {
            print("An error occurred: \(error?.localizedDescription)")
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "contextWillSaveContext:", name: NSManagedObjectContextWillSaveNotification, object: nil)
    }
    
    func contextWillSaveContext(notification: NSNotification) {
        let sender = notification.object as! NSManagedObjectContext
        if sender === self.context {
            if ((self.context?.deletedObjects.contains(self.team)) != false) {
                self.navigationController?.popViewControllerAnimated(false)
            }
        }
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        //small trick. better way is to use property change listeners to update ui.
        //some kind if mvvm framework could help
        self.nameLabel.text = self.team.name
        self.colorView.backgroundColor = self.team.color
    }
    
    // MARK: - Fetched results controller
    
    lazy var fetchedResultsController: NSFetchedResultsController = {
        let fetchRequest = NSFetchRequest(entityName: "Player")
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format:"team == %@", self.team!)
        
        let frc = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: self.context!,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        frc.delegate = self
        
        return frc
        }()
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController,
        didChangeObject object: AnyObject,
        atIndexPath indexPath: NSIndexPath?,
        forChangeType type: NSFetchedResultsChangeType,
        newIndexPath: NSIndexPath?) {
            switch type {
            case .Insert:
                self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Update:
                let cell = self.tableView.cellForRowAtIndexPath(indexPath!)
                self.tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Move:
                self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
                self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Delete:
                self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            default:
                return
            }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections.count
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            let currentSection = sections[section] as! NSFetchedResultsSectionInfo
            return currentSection.numberOfObjects
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PlayerCell", forIndexPath: indexPath) as! UITableViewCell
        let obj = fetchedResultsController.objectAtIndexPath(indexPath) as! Player
        
        cell.textLabel?.text = obj.name
        cell.detailTextLabel?.text = obj.age != nil ? obj.age!.stringValue + " years. " + obj.position! : obj.position!
        
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "PLAYERS"
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "EditTeamSegue"){
            let nav = segue.destinationViewController as! UINavigationController
            let controller = nav.viewControllers[0] as! EditTeamTableViewController
            controller.context = self.context
            controller.team = self.team
        }
    }

}

//
//  EditPlayerTableViewController.swift
//  
//
//  Created by Denis Zakharov on 13/09/15.
//
//

import UIKit
import CoreData

class EditPlayerTableViewController: UITableViewController, UITextFieldDelegate {
    
    // MARK: - Outlets & Actions
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var positionField: UITextField!
    
    @IBAction func cancelTapped(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func doneTapped(sender: AnyObject) {
        if (self.player == nil) {
            self.player = NSEntityDescription.insertNewObjectForEntityForName("Player", inManagedObjectContext: self.context!) as? Player
            self.player?.team = self.team!
        }
        self.player?.name = self.nameField.text
        self.player?.age = !self.ageField.text.isEmpty ? self.ageField.text.toInt() : nil
        self.player?.position = self.positionField.text
        
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Public properties
    
    var context: NSManagedObjectContext?
    var team: Team?
    var player: Player?
    
    // MARK: - Lifcycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameField.delegate = self
        self.ageField.delegate = self
        self.positionField.delegate = self
        
        //New player
        if (self.player == nil) {
            self.toolbarItems?.removeAtIndex(1)
            self.title = "New Player"
            self.navigationItem.rightBarButtonItem?.title = "Add"
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
        else {
            self.nameField.text = self.player?.name
            self.ageField.text = self.player?.age?.stringValue
            self.positionField.text = self.player?.position
        }

    }
    
    // MARK: - UITextFieldDelegate implementation
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField == self.nameField) {
            self.ageField.becomeFirstResponder()
        }
        else if (textField == self.ageField) {
            self.positionField.becomeFirstResponder()
        }
        else {
            self.view.endEditing(true)
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (textField == self.nameField) {
            let res = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
            self.navigationItem.rightBarButtonItem?.enabled = !res.isEmpty
        }
        return true
    }

}

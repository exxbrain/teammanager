//
//  EditTeamTableViewController.swift
//  
//
//  Created by Denis Zakharov on 13/09/15.
//
//

import UIKit
import CoreData
import ColorSlider

class EditTeamTableViewController: UITableViewController, NSFetchedResultsControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate {

    // MARK: - IBOutlets, IBActions

    @IBOutlet weak var addPlayerButton: UIBarButtonItem!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var colorView: UIView!
    
    @IBAction func cancelTapped(sender: AnyObject) {
        self.context?.rollback()
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func doneTapped(sender: AnyObject) {
        self.team?.name = self.nameField.text
        self.saveContext()
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func deleteTapped(sender: AnyObject) {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete")
        actionSheet.showInView(self.view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, didDismissWithButtonIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            self.context?.deleteObject(self.team!);
            self.saveContext()
            self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    private func saveContext() {
        var moc = self.context!
        var error: NSError? = nil
        if moc.hasChanges && !moc.save(&error) {
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
    }

    // MARK: - public properties
    
    var context : NSManagedObjectContext?
    var team: Team?
    
    // MARK: - Lifcycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nameField.delegate = self

        //New team
        if (self.team == nil) {
            self.toolbarItems?.removeAtIndex(0)
            self.title = "New Team"
            self.navigationItem.rightBarButtonItem?.title = "Add"
            self.team = NSEntityDescription.insertNewObjectForEntityForName("Team", inManagedObjectContext: self.context!) as? Team
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
        else {
            self.nameField.text = self.team?.name
        }
        
        var error: NSError? = nil
        if (fetchedResultsController.performFetch(&error) == false) {
            print("An error occurred: \(error?.localizedDescription)")
        }
        
        self.tableView.editing = true
    }
    
    override func viewWillAppear(animated: Bool) {
        //small trick. better way is to use property change listeners to update ui.
        //some kind if mvvm framework could help
        self.colorView.backgroundColor = self.team?.color
    }
    
    private func updateView() {
        self.colorView.backgroundColor = self.team?.color
        self.navigationItem.rightBarButtonItem?.enabled = !self.nameField.text.isEmpty
    }
    
    // MARK: - UITextFieldDelegate implementation

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let res = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
        self.navigationItem.rightBarButtonItem?.enabled = !res.isEmpty
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        self.navigationItem.rightBarButtonItem?.enabled = false
        return true
    }
    
    // MARK: - Fetched results controller
    
    lazy var fetchedResultsController: NSFetchedResultsController = {
        let fetchRequest = NSFetchRequest(entityName: "Player")
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchRequest.predicate = NSPredicate(format:"team == %@", self.team!)
        
        let frc = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: self.context!,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        frc.delegate = self
        
        return frc
        }()
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController,
        didChangeObject object: AnyObject,
        atIndexPath indexPath: NSIndexPath?,
        forChangeType type: NSFetchedResultsChangeType,
        newIndexPath: NSIndexPath?) {
            switch type {
            case .Insert:
                self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Update:
                let cell = self.tableView.cellForRowAtIndexPath(indexPath!)
                self.tableView.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            case .Move:
                self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
                self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
            case .Delete:
                self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            default:
                return
            }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections.count
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            let currentSection = sections[section] as! NSFetchedResultsSectionInfo
            return currentSection.numberOfObjects
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PlayerCell", forIndexPath: indexPath) as! UITableViewCell
        let obj = fetchedResultsController.objectAtIndexPath(indexPath) as! Player
        
        cell.textLabel?.text = obj.name
        cell.detailTextLabel?.text = obj.age != nil ? obj.age!.stringValue + " years. " + obj.position! : obj.position!
        
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "PLAYERS"
    }
    
        
    //MARK: - Editing
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let player = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Player
            self.context?.deleteObject(player)
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "EditPlayerSegue") {
            let nav = segue.destinationViewController as! UINavigationController
            let controller = nav.viewControllers[0] as! EditPlayerTableViewController
            controller.context = self.context
            controller.team = self.team
            if let indexPath = tableView.indexPathForSelectedRow() {
                let player = fetchedResultsController.objectAtIndexPath(indexPath) as! Player
                controller.player = player
            }
        }
        else if(segue.identifier == "CreatePlayerSegue") {
            let nav = segue.destinationViewController as! UINavigationController
            let controller = nav.viewControllers[0] as! EditPlayerTableViewController
            controller.context = self.context
            controller.team = self.team
        }
        else if(segue.identifier == "ChangeColorSegue") {
            let nav = segue.destinationViewController as! UINavigationController
            let controller = nav.viewControllers[0] as! ColorPickerViewController
            controller.team = self.team
        }
    }


}
